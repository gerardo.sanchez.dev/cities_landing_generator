# City Summary

This APP gets data from different publicly available API's to generate a summary of 10 cities, then display a simple landing page for each one of them
The basic information is obtained from [GeoDB](https://geodb.com/), the images from [Pexels](https://www.pexels.com/), and finally the summaries are generated via AI using [OpenAi](https://openai.com)

For the FrontEnd we are using Angular 15, and the BackEnd was developed via NodeJS 20, using ExpressJS and Request as the primary libraries

A live version of this site can be found on my [Server](https://octo.tdd.mx), keep in mind this is a hobby project, and all API keys are Free Tier, so it can happen that you get no images, no summary, or directly a blank page

## Running the project

We use the  [npm](https://www.npmjs.com/) package manager to install all packages from front and back end

### API

1. Navigate to folder "api"
2. Create a .env file with the same structure as .env_example and add your credentials/keys
3. Run command
```bash
npm install
```
4. Wait for packages to finish installation
5. To run the project in a "test" environment, it is recomended to use "nodemon", installed globally with the following command:
```bash
npm install -g nodemon --save
```
6. We run the BackEnd with either "node" or "nodemon" commands
```bash
nodemon index | node index
```

### FrontEnd

1. Navigate to folder "client"
2. Run command
```bash
npm install
```
3. Wait for packages to finish installation
4. Run the project with the following command
```bash
ng serve 
```