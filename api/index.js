require('dotenv').config();
const express = require('express');
const cors = require('cors');
const http = require('https');
const request = require('request');

const OpenAI = require('openai')
const openai = new OpenAI({ apiKey: process.env.OPENAI_API_KEY });


const app = express();
var corsOptions = {
  credentials: true,
  origin: "https://octo.tdd.mx"
  /* origin:"http://localhost:4200" */
}

app.use(cors(corsOptions));
/* app.use(cors()) */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

async function getCities(req, res) {

  try {
    //Construct the request for the GEODB api
    const geodbOptions = {
      method: 'GET',
      url: `https://${process.env.GEODB_API_HOST}/v1/geo/cities`,
      qs: {
        //setting a minimum population of 2,000,000 people to avoid getting small in-the-middle-of-nowhere cities
        minPopulation: '2000000',
        //Getting a total of 10 cities (max amout allowed for free version of api)
        limit: '10',
        //Country code from which we'll be getting the cities
        countryIds: 'US',
      },
      headers: {
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
        'X-RapidAPI-Host': process.env.GEODB_API_HOST
      }
    };
    request(geodbOptions, (error, response, body) => {
      if (error) {
        console.log(error)
        return res.status(500).send({
          data: null,
          meta: {
            message: 'Error fetching the cities',
            error: `${error}`
          }
        })
      }
      const cities = JSON.parse(response.body).data
      return res.status(200).send({
        data: {
          cities: cities
        },
      })
    })

  } catch (error) {
    console.log(error)
    return res.status(500).send({
      data: null,
      meta: {
        message: 'Server-side error, try again later',
        error: `${error}`
      }
    })
  }
}

async function getCityParagraph(req, res) {
  try {
    const completion = await openai.chat.completions.create({
      messages: [{
        role: "system", content: `
        Generate a 300 word text talking about the city: "${req.query.cityName}, ${req.query.regionCode}" provide your answer as a single string of text, with no added messages from you, in the event the response is not what i asked for, do not tell me, just provide as much of the response I asked for as possible`
      }],
      model: "gpt-3.5-turbo",
    });
    return res.status(200).send({
      data: completion.choices[0].message.content
    })
  } catch (error) {
    return res.status(500).send({
      message: 'Server-side error, try again later',
      error: `${error}`
    })
  }
}


async function generateSummaryHashtags(req, res) {
  try {
    const completion = await openai.chat.completions.create({
      messages: [{
        role: "system", content: `
        Generate a 30 word summary for the following city: "${req.query.cityName}, ${req.query.regionCode}", also generate 3 hashtags related to the city, provide your answer as a JSON object with the following structure: {{summary:"summary", hashTags:[hashtag_1, hashtag_2, hashtag_3]}}`
      }],
      model: "gpt-3.5-turbo",
    });
    /* const summaries = completion.choices[0].message.content */
    return res.status(200).send({
      data: JSON.parse(completion.choices[0].message.content)
    })
  } catch (error) {
    return res.status(500).send({
      data: null,
      meta: {
        message: 'Server-side error, try again later',
        error: `${error}`
      }
    })
  }

}

async function getImage(req, res) {
  try {
    const options = {
      method: 'GET',
      url: 'https://pexelsdimasv1.p.rapidapi.com/v1/search',
      qs: {
        query:`${req.query.cityName.replace(' ','-')}`,
        locale: 'en-US',
        per_page: '1',
        page: '1'
      },
      headers: {
        Authorization: process.env.PEXELS_API_KEY,
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
        'X-RapidAPI-Host': process.env.PEXELS_API_HOST
      }
    };
    request(options, (error, response, body) => {
      if (error) {
        return res.status(500).send({
          data: null,
          meta: {
            message: 'Server side error, try again later',
            error: `${error}`
          }
        })
      }
      return res.status(200).send({
        data:JSON.parse(response.body).photos
      })
    })
  } catch (error) {
    return res.status(500).send({
      data: null,
      meta: {
        message: 'Server-side error, try again later',
        error: `${error}`
      }
    })
  }
}

/* app.use('/get-cities-with-summary', getCitiesWithSummary) */
app.use('/api/get-paragraph', getCityParagraph)
app.use('/api/get-summary-hashtags', generateSummaryHashtags)
app.use('/api/get-cities', getCities)
app.use('/api/get-city-image', getImage)

app.use((req, res, next) => {
  res.status(404).send({
    message: 'Non-existing EndPoint, check documentation'
  })
})

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "https://octo.tdd.mx");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
})

const PORT = 5000;
app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`)
})