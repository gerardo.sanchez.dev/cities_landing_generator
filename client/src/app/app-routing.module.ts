import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { CitySummaryComponent } from './components/city-summary/city-summary.component';

const routes: Routes = [
  {path:'', component:MainComponent},
  {path:'city', component: CitySummaryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
