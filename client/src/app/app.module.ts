import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponentComponent } from './components/card-component/card-component.component';
import { MainComponent } from './components/main/main.component';
import { CitySummaryComponent } from './components/city-summary/city-summary.component';
import { HttpClientModule } from '@angular/common/http';
import { CardSkeletonComponent } from './components/card-skeleton/card-skeleton.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponentComponent,
    MainComponent,
    CitySummaryComponent,
    CardSkeletonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
