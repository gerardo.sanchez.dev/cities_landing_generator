import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CitySummaryService } from 'src/app/services/city-summary.service';

@Component({
  selector: 'app-card-component',
  templateUrl: './card-component.component.html',
  styleUrls: ['./card-component.component.scss']
})
export class CardComponentComponent {
  @Input() cityName = ''
  @Input() regionCode = ''
  @Input() hashTags = []
  @Input() summary = ''
  @Input() imageUrl = ''
  @Input() largeImageUrl = ''
  @Input() population = ''
  @Input() latitude = ''
  @Input() longitude = ''
  details:string=''

  showModal:boolean=false

  constructor(
    private db: CitySummaryService,
    private router: Router
  ){

  }

  navigateCityPage(){
    this.router.navigate(['city'],{queryParams:{cityName:this.cityName, regionCode: this.regionCode, population:this.population}})
  }

}
