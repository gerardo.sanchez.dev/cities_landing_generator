import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CitySummaryService } from 'src/app/services/city-summary.service';

@Component({
  selector: 'app-city-summary',
  templateUrl: './city-summary.component.html',
  styleUrls: ['./city-summary.component.scss']
})
export class CitySummaryComponent {

  cityName:string = ''
  regionCode:string = ''
  bannerImageUrl:string = ''
  population:string=''
  summary:string=''

  constructor(
    private ar: ActivatedRoute,
    private db: CitySummaryService
  ){
    this.cityName=this.ar.snapshot.queryParamMap.get('cityName')!
    this.regionCode=this.ar.snapshot.queryParamMap.get('regionCode')!
    this.population=this.ar.snapshot.queryParamMap.get('population')!
    /* this.ar.params.subscribe(params=>{
      this.cityName=params['cityName']
    }) */
  }

  ngOnInit():void{
    this.db.getCityImage(this.cityName).subscribe({
      next:(response:any)=>{
        this.bannerImageUrl=response.data[0].src.landscape
      }
    })
    this.db.getDetails(this.cityName, this.regionCode).subscribe({
      next:(response:any)=>{
        this.summary=response.data
      }
    })
  }



}
