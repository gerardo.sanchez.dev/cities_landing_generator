import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CitySummaryComponent } from './city-summary.component';

describe('CitySummaryComponent', () => {
  let component: CitySummaryComponent;
  let fixture: ComponentFixture<CitySummaryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CitySummaryComponent]
    });
    fixture = TestBed.createComponent(CitySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
