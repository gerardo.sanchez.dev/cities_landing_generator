import { Component } from '@angular/core';
import { CitySummaryService } from 'src/app/services/city-summary.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  citiesSummary:any[]=[]
  skeletons:any[]=['','','','','','','','','','']
  isLoading!:Promise<boolean>

  constructor(
    private cityService: CitySummaryService
  ){
  }

  ngOnInit():void{
    this.cityService.getCities().subscribe({
      next:(response:any)=>{
        this.citiesSummary=response.data.cities.map((city:any)=>{
          city.hashTags=['','','']
          return city
        })
        this.skeletons=[]
        for(const city of response.data.cities){
          this.cityService.getSummaryHashtags({cityName:city.name, regionCode:city.region}).subscribe({
            next:(response:any)=>{
              city.hashTags=response.data.hashTags
              city.summary=response.data.summary
            }
          })
          this.cityService.getCityImage(city.name).subscribe({
            next:(response:any)=>{
              city.imageUrl=response.data[0].src.tiny
              city.largeImageUrl=response.data[0].src.landscape
            }
          })
        }
      },
      error:(err:any)=>{
        this.isLoading=Promise.resolve(true)
      }
    })
  }

}
