import { TestBed } from '@angular/core/testing';

import { CitySummaryService } from './city-summary.service';

describe('CitySummaryService', () => {
  let service: CitySummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CitySummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
