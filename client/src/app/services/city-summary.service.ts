import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

const URL = 'https://cities-landing-generator.onrender.com/api'
/* const URL = 'http://localhost:5000/api' */
let httpOptions:any = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class CitySummaryService {

  constructor(
    private http: HttpClient
  ) { }

  getCitiesWithSummary(){
    return this.http.get(`${URL}/get-cities-with-summary`)
  }

  getCities(){
    return this.http.get(`${URL}/get-cities`)
  }
  getSummaryHashtags(params?:any){
    if(params){
      httpOptions.params=params
    }
    return this.http.get(`${URL}/get-summary-hashtags`, httpOptions)
  }
  getCityImage(cityName:string){
      httpOptions.params={cityName: cityName}
      return this.http.get(`${URL}/get-city-image`, httpOptions)
  }
  getDetails(cityName:string, regionCode: string){
    httpOptions.params={cityName: cityName, regionCode: regionCode}
    return this.http.get(`${URL}/get-paragraph`, httpOptions)
  }
  
}
